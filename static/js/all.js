function printFunction() {
  window.print();
}

var $ = jQuery.noConflict();
(function($) {
  'use strict';
  
  $('#dataTable').DataTable({
    "language": {
      "lengthMenu":  "Mostrar _MENU_ por página",
      "info":        "Mostrando _PAGE_ de _PAGES_",
      "search":      "Pesquisar:",
      "paginate": {
        "first":      "Primeira",
        "last":       "Ultima",
        "next":       "Próxima",
        "previous":   "Anterior"
      },
      "zeroRecords":    "Nada Encontrado",
    }
  });

  $('.app-nav-vicariatos li a').addClass('name');

  $('.col-search form input').focus(function() {
    $(this).css('opacity', '1');
  });

  $('.col-search form input').blur(function() {
    $(this).css('opacity', '.5');
  });

  $(".app-nav-toggle").on("click", function() {
    $(".sidebar-app").toggleClass("sidebar-app-toogle");
  });

  /* search mobile */

  $('.ico-search').on('click', function() {
    $('.col-search form').show();
  });

  $('.close-search').on('click', function () {
    $('.col-search form').hide();
  });

  $('.sidebar-app').find("a").filter(function() {
    return this.href.replace(/\/+$/, '') === window.location.href.replace(/\/+$/, '');
  }).addClass('current-active-page-url');



  /*
    * MODAL
    *
  */

  $('.form-exit').on('click', function() {
    $('.modal-form__container').removeClass('fadeInDown').addClass('fadeOutUp');
    $('.modal-form').fadeOut();
  });
  
  $('.link-feedback').on('click', function() {
    $('.modal-form__container').removeClass('fadeOutUp').addClass('fadeInDown');
    $('.modal-feedback').css('display', 'flex').fadeIn();
  });
  
  $('.link-help').on('click', function() {
    $('.modal-form__container').removeClass('fadeOutUp').addClass('fadeInDown');
    $('.modal-help').css('display', 'flex').fadeIn();
  });


  /*
    * LOAD MORE COMMENTS VIDEOS
    *
  */

    $(".item-load .item-app__item").slice(0, 10).show();

    $(".load-more").on('click', function (e) {
      e.preventDefault();
      $(".item-load .item-app__item:hidden").slice(0, 20).slideDown();
      if ($("item-load. .item-app__item:hidden").length == 0) {
          $(".load-more").fadeOut('slow');
      }
    });

    if($(".item-load .item-app__item").length < 10) {
      $(".item-load .item-app__item").show();
      $(".load-more").hide();
    };
    
    

}(jQuery));

// HISTORY BACK BUTTON
function goBack() {
  window.history.back();
}

var options = {
  valueNames: ['name']
};

var userList = new List('users', options);
