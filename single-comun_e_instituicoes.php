<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="container-single d-flex box">
  <div class="image-single">
    <?php if(has_post_thumbnail()) : ?>
    <?php
      if(has_post_thumbnail()) the_post_thumbnail();
    ?>
    <?php else : ?>
      <img style="min-width: 160px; max-width: 160px; min-height: auto" src="<?php echo get_template_directory_uri(); ?>/static/images/building.png" alt="">
    <?php endif; ?>
    <div class="close-single">
      <button onclick="goBack()" title="Voltar"><svg aria-hidden="true" data-prefix="fas" data-icon="arrow-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-arrow-left fa-w-14 fa-3x"><path fill="currentColor" d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" class=""></path></svg></button>
    </div>
  </div>
  <div class="text-single">
    <div class="edit-single">
      <?php edit_post_link('<svg aria-hidden="true" data-prefix="far" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-edit fa-w-18 fa-3x"><path fill="currentColor" d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z" class=""></path></svg>','',''); ?>
    </div>
    <header class="header-single">
      <h1><?php the_title(); ?></h1>
      <?php the_field('after_title_comun'); ?>
    </header>
    <hr> 
    <div class="content-text-single">
      <?php if( have_rows('endereco_comun_geral') ): while( have_rows('endereco_comun_geral') ): the_row(); 
      ?>
        <ul>
          <?php if(!get_sub_field('endereco_comun') && !get_sub_field('cidade_comun') && !get_sub_field('estado_comun') && !get_sub_field('pais_comun') && !get_sub_field('cep_comun') && !get_sub_field('caixa_postal_comun')): ?>
          <p></p>
          <?php else : ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 fa-3x"><path fill="currentColor" d="M192 96c-52.935 0-96 43.065-96 96s43.065 96 96 96 96-43.065 96-96-43.065-96-96-96zm0 160c-35.29 0-64-28.71-64-64s28.71-64 64-64 64 28.71 64 64-28.71 64-64 64zm0-256C85.961 0 0 85.961 0 192c0 77.413 26.97 99.031 172.268 309.67 9.534 13.772 29.929 13.774 39.465 0C357.03 291.031 384 269.413 384 192 384 85.961 298.039 0 192 0zm0 473.931C52.705 272.488 32 256.494 32 192c0-42.738 16.643-82.917 46.863-113.137S149.262 32 192 32s82.917 16.643 113.137 46.863S352 149.262 352 192c0 64.49-20.692 80.47-160 281.931z" class=""></path></svg>
            
            <?php if( get_sub_field('endereco_comun') ): ?>
              <?php the_sub_field('endereco_comun'); ?>,
            <?php endif; ?> 

            <?php if( get_sub_field('cidade_comun') ): ?>
              <?php the_sub_field('cidade_comun'); ?>, 
            <?php endif; ?> 

            <?php if( get_sub_field('cep_comun') ): ?>
              <?php the_sub_field('cep_comun'); ?>.
            <?php endif; ?>

            <?php if( get_sub_field('caixa_postal_comun') ): ?>
              <?php the_sub_field('caixa_postal_comun'); ?>
            <?php endif; ?>
          </li>
          <?php endif; ?>

          <?php if( get_field('e-mail_comun') ): ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-envelope fa-w-16 fa-3x"><path fill="currentColor" d="M464 64H48C21.5 64 0 85.5 0 112v288c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h416c8.8 0 16 7.2 16 16v41.4c-21.9 18.5-53.2 44-150.6 121.3-16.9 13.4-50.2 45.7-73.4 45.3-23.2.4-56.6-31.9-73.4-45.3C85.2 197.4 53.9 171.9 32 153.4V112c0-8.8 7.2-16 16-16zm416 320H48c-8.8 0-16-7.2-16-16V195c22.8 18.7 58.8 47.6 130.7 104.7 20.5 16.4 56.7 52.5 93.3 52.3 36.4.3 72.3-35.5 93.3-52.3 71.9-57.1 107.9-86 130.7-104.7v205c0 8.8-7.2 16-16 16z" class=""></path></svg>

            <a href="mailto:<?php the_field('e-mail_comun'); ?>">
              <?php the_field('e-mail_comun'); ?>
            </a>

            <?php
              // check if the repeater field has rows of data
              if( have_rows('outros_emails_comun') ):
                // loop through the rows of data
                  while ( have_rows('outros_emails_comun') ) : the_row();
                      // display a sub field value
                      echo "&nbsp;/&nbsp;<a href='tel:" .get_sub_field('numero_comun_others'). "'>";
                      the_sub_field('email_comun_others');
                      echo '</a>';
                  endwhile;
              else :
                  // no rows found
              endif;
            ?>
          </li>
          <?php endif; ?>
          
          <?php if( get_field('site_comun') ): ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="browser" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-browser fa-w-16 fa-3x"><path fill="currentColor" d="M464 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zM32 80c0-8.8 7.2-16 16-16h48v64H32V80zm448 352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V160h448v272zm0-304H128V64h336c8.8 0 16 7.2 16 16v48z" class=""></path></svg>  
            <a href="http://<?php the_field('site_comun'); ?>" target="_blank">
              <?php the_field('site_comun'); ?> 
            </a>
          </li>
          <?php endif; ?>

          <?php if( get_field('telefone_comun') ): ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="phone" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-phone fa-w-16 fa-3x"><path fill="currentColor" d="M487.8 24.1L387 .8c-14.7-3.4-29.8 4.2-35.8 18.1l-46.5 108.5c-5.5 12.7-1.8 27.7 8.9 36.5l53.9 44.1c-34 69.2-90.3 125.6-159.6 159.6l-44.1-53.9c-8.8-10.7-23.8-14.4-36.5-8.9L18.9 351.3C5 357.3-2.6 372.3.8 387L24 487.7C27.3 502 39.9 512 54.5 512 306.7 512 512 307.8 512 54.5c0-14.6-10-27.2-24.2-30.4zM55.1 480l-23-99.6 107.4-46 59.5 72.8c103.6-48.6 159.7-104.9 208.1-208.1l-72.8-59.5 46-107.4 99.6 23C479.7 289.7 289.6 479.7 55.1 480z" class=""></path></svg> 
            <a href="tel: <?php the_field('telefone_comun') ?>">
              <?php the_field('telefone_comun') ?>
            </a>
            <?php
              // check if the repeater field has rows of data
              if( have_rows('outros_telefones_comun') ):
                // loop through the rows of data
                  while ( have_rows('outros_telefones_comun') ) : the_row();
                      // display a sub field value
                      echo "&nbsp;/&nbsp; <a href='tel:" .get_sub_field('numero_comun_others'). "'>";
                      the_sub_field('numero_comun_others');
                      echo '</a>';
                  endwhile;
              else :
                  // no rows found
              endif;
            ?>
          </li>
          <?php endif; ?>
          
          <?php if( get_sub_field('facebook_posttype') ): ?>
          <li>
            <svg aria-hidden="true" data-prefix="fab" data-icon="facebook-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-facebook-square fa-w-14 fa-7x"><path fill="currentColor" d="M448 80v352c0 26.5-21.5 48-48 48h-85.3V302.8h60.6l8.7-67.6h-69.3V192c0-19.6 5.4-32.9 33.5-32.9H384V98.7c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9H184v67.6h60.9V480H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48z" class=""></path></svg>
            @<?php the_sub_field('facebook_posttype') ?>
          </li>
          <?php endif; ?>
          
          <?php if( get_sub_field('instagram_posttype') ): ?>
          <li>
            <i class="fab fa-instagram" aria-hidden="true"></i> 
            @<?php the_sub_field('instagram_posttype') ?>
          </li>
          <?php endif; ?>

        </ul>
      <?php endwhile; endif; ?>
    </div>
  </div>
</div>

<?php if(get_the_content()): ?>
<div class="box p-box container-single text-comun-e-inst">
  <?php the_content(); ?>
</div>
<?php endif; ?>


<?php if(get_field('pessoas_-_comunidades_instituicoes_e_pastorais')): ?>
<div class="box p-box container-single peoples-comun">
  <?php
  // check if the repeater field has rows of data
  if( have_rows('pessoas_-_comunidades_instituicoes_e_pastorais') ):
  // loop through the rows of data
  while ( have_rows('pessoas_-_comunidades_instituicoes_e_pastorais') ) : the_row();
  ?>
  <div class="item-people-comun">
    
    <?php if(get_sub_field('titulo_pessoas_posttype')): ?>
    <h4><?php the_sub_field('titulo_pessoas_posttype'); ?></h4>
    <?php endif; ?>

    <ul>
      <?php if(get_sub_field('nome_da_pessoa_posttype')): ?>
      <li>
        <div class="ico-people-comun">
          <i class="fas fa-id-badge"></i>
        </div>
        <div class="text-people-comun">
          <p><?php the_sub_field('nome_da_pessoa_posttype') ?></p>
        </div>
      </li>
      <?php endif; ?>

      <?php if(get_sub_field('e-mail_pessoa_posttype')): ?>
      <li>
        <div class="ico-people-comun">
          <i class="far fa-envelope"></i>
        </div>
        <div class="text-people-comun email-people-comun">
          <p><?php the_sub_field('e-mail_pessoa_posttype') ?></p>
        </div>
      </li>
      <?php endif; ?>

      <?php if(get_sub_field('telefone_pessoa_posttype')): ?>
      <li>
        <div class="ico-people-comun">
          <svg aria-hidden="true" data-prefix="fal" data-icon="phone" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-phone fa-w-16 fa-3x"><path fill="currentColor" d="M487.8 24.1L387 .8c-14.7-3.4-29.8 4.2-35.8 18.1l-46.5 108.5c-5.5 12.7-1.8 27.7 8.9 36.5l53.9 44.1c-34 69.2-90.3 125.6-159.6 159.6l-44.1-53.9c-8.8-10.7-23.8-14.4-36.5-8.9L18.9 351.3C5 357.3-2.6 372.3.8 387L24 487.7C27.3 502 39.9 512 54.5 512 306.7 512 512 307.8 512 54.5c0-14.6-10-27.2-24.2-30.4zM55.1 480l-23-99.6 107.4-46 59.5 72.8c103.6-48.6 159.7-104.9 208.1-208.1l-72.8-59.5 46-107.4 99.6 23C479.7 289.7 289.6 479.7 55.1 480z" class=""></path></svg>
        </div>
        <div class="text-people-comun">
          <p><?php the_sub_field('telefone_pessoa_posttype') ?></p>
        </div>
      </li>
      <?php endif; ?>

    </ul>
  </div> <!-- close-item-comun -->

  <?php
  endwhile;
  else :
    // no rows found
  endif;
  ?>
</div>
<?php endif; ?>


<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'posts_to_comun',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
  'orderby'=> 'date',
  'order' => 'ASC'
) );
if ( $connected->have_posts() ) :
?>

<?php include('template-parts/conection-padre.php') ?>

<?php 
// Prevent weirdness
wp_reset_postdata(); endif;
?>


<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'igrejas_to_comun',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
  'orderby'=> 'date',
  'order' => 'ASC'
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>

<?php include('template-parts/conection-igreja.php') ?>

<?php 
// Prevent weirdness
wp_reset_postdata(); endif;
?>

<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'comun_to_comun',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
  'orderby'=> 'date',
  'order' => 'ASC'
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>

<?php include('template-parts/conection-instituicoes.php') ?>

<?php 
// Prevent weirdness
wp_reset_postdata(); endif;
?>

<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'comun_to_pastorais_e_moviment',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
  'orderby'=> 'date',
  'order' => 'ASC'
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>

<?php include('template-parts/conection-pastorais.php') ?>

<?php 
// Prevent weirdness
wp_reset_postdata(); endif;
?>


<?php endwhile; ?>
<?php get_footer() ?>
