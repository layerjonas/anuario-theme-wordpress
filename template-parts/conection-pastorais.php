
<div class="conection-padre-igreja">
  <div class="header-hierarchy" style="display: block;">
    <p>Pastorais e Movimentos</p>
  </div>
  <ul class="item-app">
    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
    <li <?php post_class(); ?>>
      <a href="<?php the_permalink() ?>">
        <div class="col-image-item-app">
          <?php if(has_post_thumbnail()) : ?>
          <div class="image-item-app">
            <?php
              if(has_post_thumbnail()) the_post_thumbnail();
            ?>
          </div>
          <?php else : ?>
          <div class="image-item-app">
            <img style="width: 35px; height: auto" src="<?php echo get_template_directory_uri(); ?>/static/images/pastorais.png" alt="">
          </div>
          <?php endif; ?>
        </div>
        <div class="col-name-item-app">
          <h2><?php the_title(); ?></h2>
          <?php if( get_field('after_title_comun') ): ?>
            <p><?php the_field('after_title_comun'); ?></p>
          <?php endif; ?>
        </div>
      </a>
    </li>
    <?php endwhile; ?>
  </ul>
</div>