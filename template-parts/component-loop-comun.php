<li <?php post_class('item-app__item'); ?>>
  <a href="<?php the_permalink() ?>">
    <div class="col-image-item-app">
      <?php if(has_post_thumbnail()) : ?>
      <div class="image-item-app" style="background: none;">
        <?php
          if(has_post_thumbnail()) the_post_thumbnail(array(60,60));
        ?>
      </div>
      <?php else : ?>
      <div class="image-item-app">
        <i class="far fa-building"></i>
      </div>
      <?php endif; ?>
    </div>
    <div class="col-name-item-app">
      <h2><?php the_title(); ?></h2>
      <?php if( get_field('after_title_comun') ): ?>
        <p><?php the_field('after_title_comun'); ?></p>
      <?php endif; ?>
    </div>

    <?php if(get_field('e-mail_comun')): ?>
      <div class="e-mail-item-app d-none d-lg-block">
        <p><?php the_field('e-mail_comun') ?></p>
      </div>
    <?php else: ?>
      <div class="e-mail-item-app d-none d-lg-block">
        <p></p>
      </div>
    <?php endif; ?>
    
    <?php if(get_field('telefone_comun')): ?>
      <div class="phone-item-app d-none d-sm-block">
        <p><?php the_field('telefone_comun') ?></p>
      </div>
    <?php else: ?>
      <div class="phone-item-app d-none d-sm-block">
        <p></p>
      </div>
    <?php endif; ?>

  </a>
</li>

