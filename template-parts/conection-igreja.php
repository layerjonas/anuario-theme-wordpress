

<div class="conection-padre-igreja">
  <div class="header-hierarchy" style="display: block;">
    <p>Igreja</p>
  </div>
  <ul class="item-app">
    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
    <li <?php post_class(); ?>>
      <a href="<?php the_permalink() ?>">
        <div class="col-image-item-app">
          <?php if(has_post_thumbnail()) : ?>
          <div class="image-item-app">
            <?php
              if(has_post_thumbnail()) the_post_thumbnail();
            ?>
          </div>
          <?php else : ?>
          <div class="image-item-app">
            <svg id="icon-church" data-name="icon-church" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><defs><style>.cls-1{fill:#263345;}.cls-2{fill:#fff;fill-rule:evenodd;}</style></defs><title>icon-church</title><rect class="cls-1" width="100" height="100" rx="50" ry="50"></rect><path class="cls-2" d="M48.75,13.21h2.4v4.57h5.07v2.39H51.14v4.19l-1.2,1.2-1.2-1.2V20.18H43.68V17.78h5.07V13.21Zm-10.51,40L19.75,60.68c-1.33.5-.78,2.07.69,1.48l2-.81V78.79h22V61.84h0L50,56.22h0l5.64,5.63V78.79H77.54l0-17.44,2,.81c1.47.6,2-1,.69-1.48L61.77,53.16V45.41L49.95,33.57,38.23,45.3v7.87Zm-3,3,0,22.66,0-22.66Zm41,4.7L61.77,54.92,76,60.72l.3.12ZM49.94,27.71,34.71,43a1.3,1.3,0,1,0,1.85,1.84l13.39-13.4V27.71L65.18,43a1.3,1.3,0,1,1-1.85,1.84L49.95,31.44V27.71ZM27.84,64.34l2.48-2.5,2.5,2.5v7.52h-5V64.34ZM47.51,43.2,50,40.7l2.5,2.5v7.52h-5V43.2ZM67.18,64.34l2.48-2.5,2.5,2.5v7.52h-5V64.34Z"></path></svg>
          </div>
          <?php endif; ?>
        </div>
        <div class="col-name-item-app">
          <h2><?php the_field('prefixer_name') ?> <?php the_title(); ?></h2>
          <?php if( get_field('after_title_igreja') ): ?>
            <p><?php the_field('after_title_igreja'); ?></p>
          <?php endif; ?>
        </div>
      </a>
    </li>
    <?php endwhile; ?>
  </ul>
</div>