

<div class="conection-padre-igreja">
  <div class="header-hierarchy" style="display: block;">
    <p>Padres</p>
  </div>
  <ul class="item-app">
    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
    <li <?php post_class(); ?>>
      <a href="<?php the_permalink() ?>">
        <div class="col-image-item-app">
          <?php if(has_post_thumbnail()) : ?>
          <div class="image-item-app">
            <?php
              if(has_post_thumbnail()) the_post_thumbnail();
            ?>
          </div>
          <?php else : ?>
          <div class="image-item-app">
            <svg id="icon-sacerdote" data-name="icon-sacerdote" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><defs><style>.cls-1{fill:#263345;}.cls-2{fill:#fff;}</style></defs><title>icon</title><rect class="cls-1" width="100" height="100" rx="50" ry="50"></rect><path class="cls-2" d="M72.91,58.08c-1.44-3.18-10.2-5.94-10.2-5.94C58,50.49,58,48.85,58,48.85c-9.19,18.11-16.17,0-16.17,0-.64,2.44-10.09,5.31-10.09,5.31a9.13,9.13,0,0,0-3.93,2.66c-4.09,6.05-4.57,19.54-4.57,19.54.06,3.08,1.39,3.4,1.39,3.4C34,84,48.78,84.74,48.78,84.74a70.2,70.2,0,0,0,26.15-4.3c1.6-1,1.64-1.8,1.64-1.8C77.69,69,72.91,58.08,72.91,58.08ZM56.67,75.15H52.18v4.58h-3.9V75.15H43.74v-3.9h4.54V66.81h3.9v4.44h4.49v3.9Z"></path><path class="cls-2" d="M39.53,39.28c.7,5.82,5.71,11.85,10.26,11.85,5.22,0,10.19-6.34,11-11.85a2.68,2.68,0,0,0,.95-1.86s1.12-4-.36-3.57c.51-1.53,2.22-7.53-1.09-11.26a10.5,10.5,0,0,0-5.32-3.23,4.26,4.26,0,0,0-.42-.33,2.91,2.91,0,0,1,.21.27l-.66-.17c-.21-.21-.43-.43-.7-.67a4.74,4.74,0,0,1,.5.63l-.3-.07a5.46,5.46,0,0,0-.64-.8s.11.21.25.54c-.69-.51-2.08-1.69-2.08-3a3.93,3.93,0,0,0-1.39,1.15,3.57,3.57,0,0,1,1.09-1.65,3.92,3.92,0,0,0-1.11.93,3.54,3.54,0,0,0-1.67,2L47.73,18a4.8,4.8,0,0,1,.63-1,4.54,4.54,0,0,0-.76.95l-.61-.3a4.87,4.87,0,0,1,.74-1,4.65,4.65,0,0,0-.67.68c.1-.39.08-.84-1.14.49,0,0-5.5,2.38-7.09,7.33,0,0-.94,2.23.3,8.79-1.77-.84-.56,3.48-.56,3.48A2.64,2.64,0,0,0,39.53,39.28Zm-.11-2.34h0ZM49.53,16.39a3.58,3.58,0,0,0-.61,1.4l-.19-.08A3.08,3.08,0,0,1,49.53,n16.39Z"></path></svg>
          </div>
          <?php endif; ?>
        </div>
        <div class="col-name-item-app">
          <h2><?php the_field('prefixer_name') ?> <?php the_title(); ?></h2>
          <?php if( get_field('after_title_padre') ): ?>
            <p><?php the_field('after_title_padre'); ?></p>
          <?php endif; ?>
        </div>
      </a>
    </li>
    <?php endwhile; ?>
  </ul>
</div>


