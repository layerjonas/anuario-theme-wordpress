<?php


  // prevent file from being loaded directly
  if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
  }
  $anuario_template_directory = get_template_directory();

  // -------------------------------------------
  // classes
  // -------------------------------------------
  require $anuario_template_directory . '/includes/classes/class-odin-thumbnail-resizer.php';
  require $anuario_template_directory . '/includes/classes/class-wp-bootstrap-navwalker.php';

  // -------------------------------------------
  // functions
  // -------------------------------------------
  require $anuario_template_directory . '/includes/functions/add-comments-disqus.php';
  require $anuario_template_directory . '/includes/functions/add-googleanalytics.php';
  require $anuario_template_directory . '/includes/functions/add-pagination.php';
  require $anuario_template_directory . '/includes/functions/add-scripts-style.php';
  // require $anuario_template_directory . '/includes/functions/add-thumbnail-admin-columns.php';
  require $anuario_template_directory . '/includes/functions/add-theme-suport.php';
  require $anuario_template_directory . '/includes/functions/change-text-size.php';
  require $anuario_template_directory . '/includes/functions/custom-post-thumbnail.php';
  require $anuario_template_directory . '/includes/functions/customize-login-admin.php';
  require $anuario_template_directory . '/includes/functions/register-menus.php';
  require $anuario_template_directory . '/includes/functions/remove-junk-header.php';
  require $anuario_template_directory . '/includes/functions/remove-theme-suport.php';
  require $anuario_template_directory . '/includes/functions/send-mail-post-pending.php';


  // CSS PARA LOGIN

  function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
    // wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
  }
  add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );


  /**
   * Rename "Posts" to "Padres"
   *
   * @link http://new2wp.com/snippet/change-wordpress-posts-post-type-news/
   */
  add_action( 'admin_menu', 'pilau_change_post_menu_label' );
  add_action( 'init', 'pilau_change_post_object_label' );
  function pilau_change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Clero';
    $submenu['edit.php'][5][0] = 'Todos os Cleros';
    $submenu['edit.php'][10][0] = 'Adicionar Clero';
    // $submenu['edit.php'][16][0] = 'Tags';
    echo '';
  }
  function pilau_change_post_object_label() {
    global $wp_post_types;
    // $labels = &$wp_post_types['post']->labels;
    // $labels->name = 'Padres';
    // $labels->singular_name = 'Padre';
    // $labels->add_new = 'Adicionar Padre';
    // $labels->add_new_item = 'Adicionar Novo Padre';
    // $labels->edit_item = 'Editar Padre';
    // $labels->new_item = 'Padre';
    // $labels->view_item = 'Ver Padres';
    // $labels->search_items = 'Pesquisar';
    // $labels->not_found = 'Nada encontrado';
    // $labels->not_found_in_trash = 'Nada na lixeira';
  }

  function my_connection_types() {

    // RELAÇÃO CLERO IGREJA
    p2p_register_connection_type( array(
      'name' => 'posts_to_igrejas',
      'from' => 'post',
      'to' => 'igreja',
      'reciprocal' => true,
      'title' => 'Relacionamento Igreja - Padre'
    ) );

    p2p_register_connection_type( array(
      'name' => 'igrejas_to_comun',
      'from' => 'igreja',
      'to' => 'comun_e_instituicoes',
      'reciprocal' => true,
      'title' => 'Relacionamento Igreja - Comunidades e Instituições'
    ) );

    // RELAÇÃO CLERO INSTITUIÇÕES
    p2p_register_connection_type( array(
      'name' => 'posts_to_comun',
      'from' => 'post',
      'to' => 'comun_e_instituicoes',
      'reciprocal' => true,
      'title' => 'Relacionamento Clero - Instituições'
    ) );

    // RELAÇÃO COMU E COMU
    p2p_register_connection_type( array(
      'name' => 'comun_to_comun',
      'from' => 'comun_e_instituicoes',
      'to' => 'comun_e_instituicoes',
      'reciprocal' => true,
      'title' => 'Relacionamento Instituições - Instituições'
    ) );

    // RELAÇÃO CLERO PASTORAIS
    p2p_register_connection_type( array(
      'name' => 'posts_to_pastorais_e_moviment',
      'from' => 'post',
      'to' => 'pastorais_e_moviment',
      'reciprocal' => true,
      'title' => 'Relacionamento Clero - Pastorais'
    ) );

    // RELAÇÃO COMUN PASTORAIS
    p2p_register_connection_type( array(
      'name' => 'comun_to_pastorais_e_moviment',
      'from' => 'comun_e_instituicoes',
      'to' => 'pastorais_e_moviment',
      'reciprocal' => true,
      'title' => 'Relacionamento Instituições - Pastorais'
    ) );


    // RELAÇÃO IGREJA TO IGREJA
    p2p_register_connection_type( array(
      'name' => 'igreja_to_igreja',
      'from' => 'igreja',
      'to' => 'igreja',
      'reciprocal' => true,
      'title' => 'Relacionamento Paróquia - Capela'
    ) );



  }
  add_action( 'p2p_init', 'my_connection_types' );


  // DIRECIONAR LOGIN PARA HOME
  function admin_default_page() {
    return '/';
  }
  add_filter('login_redirect', 'admin_default_page');


  // LINK PARA HOME - LOGIN
  add_filter( 'login_headerurl', 'custom_loginlogo_url' );
  function custom_loginlogo_url($url) {
      return 'https://anuario.bsb.br';
  }


  /*
   * MOST_VIEW_SIDEBAR_BLOG
   */

  if ( ! function_exists( 'tutsup_session_start' ) ) {
      // Cria a função
      function tutsup_session_start() {
          // Inicia uma sessão PHP
          if ( ! session_id() ) session_start();
      }
      // Executa a ação
      add_action( 'init', 'tutsup_session_start' );
  }

  // Verifica se não existe nenhuma função com o nome tp_count_post_views
  if ( ! function_exists( 'tp_count_post_views' ) ) {
    // Conta os views do post
    function tp_count_post_views () {
      // Garante que vamos tratar apenas de posts
      if ( is_single() ) {
        // Precisamos da variável $post global para obter o ID do post
        global $post;
        // Se a sessão daquele posts não estiver vazia
        if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {
          // Cria a sessão do posts
          $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;

          // Cria ou obtém o valor da chave para contarmos
          $key = 'tp_post_counter';
          $key_value = get_post_meta( $post->ID, $key, true );

          // Se a chave estiver vazia, valor será 1
          if ( empty( $key_value ) ) { // Verifica o valor
              $key_value = 1;
              update_post_meta( $post->ID, $key, $key_value );
          } else {
              // Caso contrário, o valor atual + 1
              $key_value += 1;
              update_post_meta( $post->ID, $key, $key_value );
          } // Verifica o valor
        } // Checa a sessão
      } // is_single
      return;
    }
    add_action( 'get_header', 'tp_count_post_views' );
  }



/* NOME E SOBRENOME NO CADASTRO */
add_action( 'register_form', 'myplugin_register_form' );
function myplugin_register_form() {

$first_name = ( ! empty( $_POST['first_name'] ) ) ? trim( $_POST['first_name'] ) : '';
$last_name = ( ! empty( $_POST['last_name'] ) ) ? trim( $_POST['last_name'] ) : '';
?>
<p>
  <label for="first_name"><?php _e( 'Nome', 'mydomain' ) ?><br />
    <input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" size="25" />
  </label>
</p>

<p>
  <label for="last_name"><?php _e( 'Sobrenome', 'mydomain' ) ?><br />
    <input type="text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" size="25" />
  </label>
</p>
<?php
}

//2. Add validation. In this case, we make sure first_name is required.
add_filter( 'registration_errors', 'myplugin_registration_errors', 10, 3 );
function myplugin_registration_errors( $errors, $sanitized_user_login, $user_email ) {
  if ( empty( $_POST['first_name'] ) || ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) {
      $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'mydomain' ) );
  }
  if ( empty( $_POST['last_name'] ) || ! empty( $_POST['last_name'] ) && trim( $_POST['first_name'] ) == '' ) {
      $errors->add( 'last_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'mydomain' ) );
  }
  return $errors;
}

//3. Finally, save our extra registration user meta.
add_action( 'user_register', 'myplugin_user_register' );
function myplugin_user_register( $user_id ) {
  if ( ! empty( $_POST['first_name'] ) ) {
    update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
    update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
  }
}


/* remove itens menu subscriber */
function remove_menu_items() {
  if( current_user_can('subscriber' ) ): //slug do role
      remove_menu_page('index.php' ); //Painel
      remove_menu_page('edit.php'); // Posts
      remove_menu_page('upload.php'); // Mídia
      remove_menu_page('edit.php?post_type=page'); // Páginas
      remove_menu_page('jetpack'); // Páginas
  endif;
}
add_action( 'admin_menu', 'remove_menu_items' );
