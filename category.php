<?php
  get_header();

  $category = get_query_var('cat');
  $current_cat = get_category($cat);
  $currentCategory = $current_cat->slug;
?>

<div class="header-content-app">
  <h3><?php single_cat_title(); ?></h3>
  <?php // echo get_category_description(); ?>
</div>    

<div class="container-item-app">
  <ul class="item-app item-load">

    <?php
      $query_padres_general = new WP_Query(
        array(
          'category_name' => "bispos+$currentCategory",
          'post_type' => 'post',
          'posts_per_page' => -1,
          'orderby'=> 'title',
          'order' => 'ASC'
        )
      );
      if($query_padres_general->have_posts()): while($query_padres_general->have_posts()): $query_padres_general->the_post();

      $do_not_duplicate[] = $post->ID;
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-padre' ); ?>
    <?php
      endwhile; wp_reset_query();
      endif;
    ?>

    <?php
      $query_padres_general = new WP_Query(
        array(
          'category_name' => $currentCategory,
          'post_type' => 'post',
          'posts_per_page' => -1,
          'orderby'=> 'title',
          'order' => 'ASC'
        )
      );
      if($query_padres_general->have_posts()): while($query_padres_general->have_posts()): $query_padres_general->the_post();

      if ( in_array( $post->ID, $do_not_duplicate ) ) continue; 
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-padre' ); ?>
    <?php
      endwhile; wp_reset_query();
      endif;
    ?>
    
    <?php
      $query_igreja_general = new WP_Query(
        array(
          'category_name' => $currentCategory,
          'post_type' => 'igreja',
          'posts_per_page' => -1,
          'orderby'=> 'title',
          'order' => 'ASC'
        )
      );
      if($query_igreja_general->have_posts()): while($query_igreja_general->have_posts()): $query_igreja_general->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-igreja' ); ?>
    <?php
      endwhile; wp_reset_query();
      endif;
    ?>
    
    <?php
      $query_comun_general = new WP_Query(
        array(
          'category_name' => $currentCategory,
          'post_type' => 'comun_e_instituicoes',
          'posts_per_page' => -1,
          'orderby'=> 'title',
          'order' => 'ASC'
        )
      );
      if($query_comun_general->have_posts()): while($query_comun_general->have_posts()): $query_comun_general->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-comun' ); ?>
    <?php
      endwhile; wp_reset_query();
      endif;
    ?>
    
    <?php
      $query_pastorais_general = new WP_Query(
        array(
          'category_name' => $currentCategory,
          'post_type' => 'pastorais_e_moviment',
          'posts_per_page' => -1,
          'orderby'=> 'title',
          'order' => 'ASC'
        )
      );
      if($query_pastorais_general->have_posts()): while($query_pastorais_general->have_posts()): $query_pastorais_general->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-pastorais' ); ?>
    <?php
      endwhile; wp_reset_query();
      endif;
    ?>

  </ul>

  <button class="btn load-more">Abrir mais <svg aria-hidden="true" data-prefix="far" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M441.9 167.3l-19.8-19.8c-4.7-4.7-12.3-4.7-17 0L224 328.2 42.9 147.5c-4.7-4.7-12.3-4.7-17 0L6.1 167.3c-4.7 4.7-4.7 12.3 0 17l209.4 209.4c4.7 4.7 12.3 4.7 17 0l209.4-209.4c4.7-4.7 4.7-12.3 0-17z" class=""></path></svg></button>

</div>

<?php get_footer(); ?>
