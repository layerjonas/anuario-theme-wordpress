<?php get_header(); ?>

<div class="header-content-app">
  <h3>Clero</h3>
</div>

<div class="container-item-app">
  
  <ul class="item-app item-load">
    <?php
      $query_padres_general = new WP_Query(
        array(
          'posts_per_page' => -1,
          'orderby'=> 'date',
          'category_name' => 'bispos',
          'order' => 'ASC'
        )
      );
      if($query_padres_general->have_posts()): while($query_padres_general->have_posts()): $query_padres_general->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-padre' ); ?>
    <?php endwhile;?> 
    <?php endif; ?>

    <?php
      $query_padres_general = new WP_Query(
        array(
          'posts_per_page' => -1,
          'category_name' => 'padres',
          'orderby'=> 'date',
          'order' => 'ASC'
        )
      );
      if($query_padres_general->have_posts()): while($query_padres_general->have_posts()): $query_padres_general->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-padre' ); ?>
    <?php endwhile; wp_reset_query(); endif; ?>

    <?php
      $query_padres_general = new WP_Query(
        array(
          'posts_per_page' => -1, 
          'category_name' => 'diaconos',
          'orderby'=> 'date',
          'order' => 'ASC'
        )
      );
      if($query_padres_general->have_posts()): while($query_padres_general->have_posts()): $query_padres_general->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-padre' ); ?>
    <?php endwhile; wp_reset_query(); endif; ?>
  </ul>

  <button class="btn load-more">Abrir mais <svg aria-hidden="true" data-prefix="far" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M441.9 167.3l-19.8-19.8c-4.7-4.7-12.3-4.7-17 0L224 328.2 42.9 147.5c-4.7-4.7-12.3-4.7-17 0L6.1 167.3c-4.7 4.7-4.7 12.3 0 17l209.4 209.4c4.7 4.7 12.3 4.7 17 0l209.4-209.4c4.7-4.7 4.7-12.3 0-17z" class=""></path></svg></button>

</div>

<?php get_footer(); ?>
