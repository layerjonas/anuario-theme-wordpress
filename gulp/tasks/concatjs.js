/*--------------------------------------------------------------
    CONCATENATE ALL FILES .JS
--------------------------------------------------------------*/
var gulp    = require('gulp');
var concat  = require('gulp-concat');

gulp.task('concatjs', function() {
  return gulp.src([
	  // BOOTSTRAP

	  // FUNCTIONS
	  '../src/js/functions/general.js'
	])
  .pipe(concat('all.js'))
  .pipe(gulp.dest('../static/js/'));
});
