/*--------------------------------------------------------------
    PERFOMS TAKS SASS, CONCATJS
--------------------------------------------------------------*/
var gulp = require('gulp');

// gulp.task('default', ['sass', 'concatjs', 'cssmin', 'jsmin'], function(){
gulp.task('default', ['sass', 'concatjs', 'cssmin', 'browser-sync'], function(){
  gulp.watch('../src/sass/**/*.scss', ['sass']);
  gulp.watch('../static/css/style.css', ['cssmin']);
  gulp.watch('../src/js/**/*.js', ['concatjs']);
});