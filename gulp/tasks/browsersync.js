var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;

// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
  //watch files
  var files = [
    '../src/js/**/*.js',
    '../src/sass/**/*.scss',
    '../*.php'
  ];

  //initialize browsersync
  browserSync.init(files, {
  //browsersync with a php server
  proxy: "http://anuario-igreja.local/",
  notify: false
  });
});
