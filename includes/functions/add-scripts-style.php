<?php

/*--------------------------------------------------------------
	CARREGANDO ESTILOS E SCRIPTS
--------------------------------------------------------------*/
add_action('wp_enqueue_scripts', 'btwp_enqueue_scripts');

function btwp_enqueue_scripts() {

	// Carrega jQuery nativo do WordPress
  wp_dequeue_script('jquery');
  wp_enqueue_script('jquery', false, array(), false, true);
  wp_enqueue_script('jquery-core', false, array(), false, true);
  wp_enqueue_script('jquery-migrate', false, array(), false, true);

  // wp_register_style('style-bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css');
  // wp_enqueue_style( 'style-bootstrap');

  // Carrega Google Fonts
  wp_register_style('style-roboto', 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700');
  wp_enqueue_style( 'style-roboto');

	// Carrega os estilos CSS - DESENVOLVIMENTO
	// wp_enqueue_style('all-css', get_stylesheet_directory_uri() . '/static/css/style.css');

  // wp_enqueue_style( 'anuario-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	// Carrega os estilos CSS - FINAL
	wp_enqueue_style('all-css', get_stylesheet_directory_uri() . '/static/css/style.min.css', array(), '1.0.5');

  // Internet Explorer specific stylesheet.
  // wp_enqueue_style( 'btwp-ie8', get_stylesheet_directory_uri() . '/static/css/ie8.css', array(), '20170209' );
  // wp_style_add_data( 'btwp-ie8', 'conditional', 'lte IE 8' );


	// Internet Explorer HTML5 support
  // wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/static/js/html5shiv.min.js', array(), '3.7.3', false);
  // wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

  // Internet Explorer 8 media query support
  // wp_enqueue_script( 'respond', get_template_directory_uri() . '/static/js/respond.min.js', array(), '1.4.2', false);
  // wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

  // wp_enqueue_script('js-popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array(), null, true);
  // wp_enqueue_script('js-bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array(), null, true);

  // Carrega scripts JS - DESENVOLVIMENTO
	wp_enqueue_script('table-js','https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js', array(), null, true);
	wp_enqueue_script('all-js', get_stylesheet_directory_uri() . '/static/js/all.js', array(), null, true);
  //
  // wp_enqueue_script('main-js',
  //   get_template_directory_uri().'/assets/js/main.js',
  //   array('jquery'),
  //   '',
  //   true
  // );

	// Carrega scripts JS - FINAL
	//wp_enqueue_script('scripts-js', get_stylesheet_directory_uri() . '/static/js/all.min.js', array(), null, true);

}
