<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="container-single d-flex box">
  <div class="image-single">
    <?php if(has_post_thumbnail()) : ?>
    <?php
      if(has_post_thumbnail()) the_post_thumbnail();
    ?>
    <?php else : ?>
      <svg id="icon-church" data-name="icon-church" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><defs><style>.cls-1{fill:#263345;}.cls-2{fill:#fff;fill-rule:evenodd;}</style></defs><title>icon-church</title><rect class="cls-1" width="100" height="100" rx="50" ry="50"></rect><path class="cls-2" d="M48.75,13.21h2.4v4.57h5.07v2.39H51.14v4.19l-1.2,1.2-1.2-1.2V20.18H43.68V17.78h5.07V13.21Zm-10.51,40L19.75,60.68c-1.33.5-.78,2.07.69,1.48l2-.81V78.79h22V61.84h0L50,56.22h0l5.64,5.63V78.79H77.54l0-17.44,2,.81c1.47.6,2-1,.69-1.48L61.77,53.16V45.41L49.95,33.57,38.23,45.3v7.87Zm-3,3,0,22.66,0-22.66Zm41,4.7L61.77,54.92,76,60.72l.3.12ZM49.94,27.71,34.71,43a1.3,1.3,0,1,0,1.85,1.84l13.39-13.4V27.71L65.18,43a1.3,1.3,0,1,1-1.85,1.84L49.95,31.44V27.71ZM27.84,64.34l2.48-2.5,2.5,2.5v7.52h-5V64.34ZM47.51,43.2,50,40.7l2.5,2.5v7.52h-5V43.2ZM67.18,64.34l2.48-2.5,2.5,2.5v7.52h-5V64.34Z"></path></svg>
    <?php endif; ?>
    <div class="close-single">
      <button onclick="goBack()" title="Voltar"><svg aria-hidden="true" data-prefix="fas" data-icon="arrow-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-arrow-left fa-w-14 fa-3x"><path fill="currentColor" d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" class=""></path></svg></button>
    </div>
  </div>
  <div class="text-single">
    <div class="edit-single">
      <?php edit_post_link('<svg aria-hidden="true" data-prefix="far" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-edit fa-w-18 fa-3x"><path fill="currentColor" d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z" class=""></path></svg>','',''); ?>
    </div>
    <header class="header-single">
      <h1><?php the_field('prefixer_name') ?> <?php the_title(); ?></h1>
      <?php the_field('after_title_igreja'); ?>
    </header>
    <hr> 
    <div class="content-text-single">
      <?php if( have_rows('endereco_igreja_geral') ): while( have_rows('endereco_igreja_geral') ): the_row();
      ?>
        <ul>
          <?php if( get_sub_field('data_de_fundacao_igreja') ): ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 fa-3x"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
            <?php the_sub_field('data_de_fundacao_igreja'); ?>
          </li>
          <?php endif; ?>

          <?php if(!get_sub_field('endereco_igreja') && !get_sub_field('cidade_igreja') && !get_sub_field('estado_igreja') && !get_sub_field('pais_igreja') && !get_sub_field('cep_igreja') && !get_sub_field('caixa_postal_igreja')): ?>
          <?php else : ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 fa-3x"><path fill="currentColor" d="M192 96c-52.935 0-96 43.065-96 96s43.065 96 96 96 96-43.065 96-96-43.065-96-96-96zm0 160c-35.29 0-64-28.71-64-64s28.71-64 64-64 64 28.71 64 64-28.71 64-64 64zm0-256C85.961 0 0 85.961 0 192c0 77.413 26.97 99.031 172.268 309.67 9.534 13.772 29.929 13.774 39.465 0C357.03 291.031 384 269.413 384 192 384 85.961 298.039 0 192 0zm0 473.931C52.705 272.488 32 256.494 32 192c0-42.738 16.643-82.917 46.863-113.137S149.262 32 192 32s82.917 16.643 113.137 46.863S352 149.262 352 192c0 64.49-20.692 80.47-160 281.931z" class=""></path></svg>
            
            <?php if( get_sub_field('endereco_igreja') ): ?>
              <?php the_sub_field('endereco_igreja'); ?>,
            <?php endif; ?> 

            <?php if( get_sub_field('cidade_igreja') ): ?>
              <?php the_sub_field('cidade_igreja'); ?>, 
            <?php endif; ?>

            <?php if( get_sub_field('cep_igreja') ): ?>
              <?php the_sub_field('cep_igreja'); ?>.
            <?php endif; ?>

            <?php if( get_sub_field('caixa_postal_igreja') ): ?>
              <?php the_sub_field('caixa_postal_igreja'); ?>
            <?php endif; ?>
          </li>
          <?php endif; ?>

          <?php if( get_field('e-mail_igreja') ): ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-envelope fa-w-16 fa-3x"><path fill="currentColor" d="M464 64H48C21.5 64 0 85.5 0 112v288c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h416c8.8 0 16 7.2 16 16v41.4c-21.9 18.5-53.2 44-150.6 121.3-16.9 13.4-50.2 45.7-73.4 45.3-23.2.4-56.6-31.9-73.4-45.3C85.2 197.4 53.9 171.9 32 153.4V112c0-8.8 7.2-16 16-16zm416 320H48c-8.8 0-16-7.2-16-16V195c22.8 18.7 58.8 47.6 130.7 104.7 20.5 16.4 56.7 52.5 93.3 52.3 36.4.3 72.3-35.5 93.3-52.3 71.9-57.1 107.9-86 130.7-104.7v205c0 8.8-7.2 16-16 16z" class=""></path></svg>

            <a href="mailto:<?php the_field('e-mail_igreja'); ?>">
              <?php the_field('e-mail_igreja'); ?>
            </a>
            <?php
              // check if the repeater field has rows of data
              if( have_rows('outros_emails_igreja') ):
                // loop through the rows of data
                  while ( have_rows('outros_emails_igreja') ) : the_row();
                      // display a sub field value
                      echo "&nbsp;/&nbsp;<a href='mailto:" .get_sub_field('email_igreja_others'). "'>";
                      the_sub_field('email_igreja_others');
                      echo '</a>';
                  endwhile;
              else :
                  // no rows found
              endif;
            ?>
          </li>
          <?php endif; ?>
          
          <?php if( get_field('site_igreja') ): ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="browser" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-browser fa-w-16 fa-3x"><path fill="currentColor" d="M464 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zM32 80c0-8.8 7.2-16 16-16h48v64H32V80zm448 352c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V160h448v272zm0-304H128V64h336c8.8 0 16 7.2 16 16v48z" class=""></path></svg>
            <a href="http://<?php the_field('site_igreja'); ?>" target="_blank">
              <?php the_field('site_igreja'); ?>
            </a>
          </li>
          <?php endif; ?>

          <?php if( get_field('telefone_igreja') ): ?>
          <li>
            <svg aria-hidden="true" data-prefix="fal" data-icon="phone" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-phone fa-w-16 fa-3x"><path fill="currentColor" d="M487.8 24.1L387 .8c-14.7-3.4-29.8 4.2-35.8 18.1l-46.5 108.5c-5.5 12.7-1.8 27.7 8.9 36.5l53.9 44.1c-34 69.2-90.3 125.6-159.6 159.6l-44.1-53.9c-8.8-10.7-23.8-14.4-36.5-8.9L18.9 351.3C5 357.3-2.6 372.3.8 387L24 487.7C27.3 502 39.9 512 54.5 512 306.7 512 512 307.8 512 54.5c0-14.6-10-27.2-24.2-30.4zM55.1 480l-23-99.6 107.4-46 59.5 72.8c103.6-48.6 159.7-104.9 208.1-208.1l-72.8-59.5 46-107.4 99.6 23C479.7 289.7 289.6 479.7 55.1 480z" class=""></path></svg>
             
            <a href="tel:<?php the_field('telefone_igreja') ?>">
              <?php the_field('telefone_igreja') ?>
            </a>
            <?php
              // check if the repeater field has rows of data
              if( have_rows('outros_telefones_igreja') ):
                // loop through the rows of data
                  while ( have_rows('outros_telefones_igreja') ) : the_row();
                      // display a sub field value
                      echo "&nbsp;/&nbsp;<a href='tel:" .get_sub_field('numero_igreja_others'). "'>";
                      the_sub_field('numero_igreja_others');
                      echo '</a>';
                  endwhile;
              else :
                  // no rows found
              endif;
            ?>
          </li>
          <?php endif; ?>

        </ul>

        <ul class="social-list-igreja">
          <?php if( get_sub_field('facebook_igreja') ): ?>
          <li>
            <a target="_blank" href="https://www.facebook.com/<?php the_sub_field('facebook_igreja') ?>">
              <i class="fab fa-facebook"></i>
            </a>
          </li>
          <?php endif; ?>
          <?php if( get_sub_field('instagram_igreja') ): ?>
          <li>
            <a target="_blank" href="https://www.instagram.com/<?php the_sub_field('instagram_igreja') ?>">
              <i class="fab fa-instagram"></i>
            </a>
          </li>
          <?php endif; ?>
          <?php if( get_sub_field('twitter_igreja') ): ?>
          <li>
            <a target="_blank" href="https://www.twitter.com/<?php the_sub_field('twitter_igreja') ?>">
              <i class="fab fa-twitter"></i>
            </a>
          </li>
          <?php endif; ?>
        </ul>
      <?php endwhile; endif; ?>
    </div>
  </div>
</div>

<?php if(get_field('capelas_igrejas')): ?>
<div class="box p-box container-single peoples-comun">
  <?php
  // check if the repeater field has rows of data
  if( have_rows('capelas_igrejas') ):
  // loop through the rows of data
  while ( have_rows('capelas_igrejas') ) : the_row();
  ?>
  <div class="item-people-comun">
    <h4><?php the_sub_field('nome_da_capela_igreja'); ?></h4>
    <ul>

      <?php if(get_sub_field('e-mail_capela_igreja')): ?>
      <li>
        <div class="ico-people-comun">
          <i class="far fa-envelope"></i>
        </div>
        <div class="text-people-comun email-people-comun">
          <p><?php the_sub_field('e-mail_capela_igreja') ?></p>
        </div>
      </li>
      <?php endif; ?>

      <?php if(get_sub_field('telefone_capela_igreja')): ?>
      <li>
        <div class="ico-people-comun">
          <svg aria-hidden="true" data-prefix="fal" data-icon="phone" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-phone fa-w-16 fa-3x"><path fill="currentColor" d="M487.8 24.1L387 .8c-14.7-3.4-29.8 4.2-35.8 18.1l-46.5 108.5c-5.5 12.7-1.8 27.7 8.9 36.5l53.9 44.1c-34 69.2-90.3 125.6-159.6 159.6l-44.1-53.9c-8.8-10.7-23.8-14.4-36.5-8.9L18.9 351.3C5 357.3-2.6 372.3.8 387L24 487.7C27.3 502 39.9 512 54.5 512 306.7 512 512 307.8 512 54.5c0-14.6-10-27.2-24.2-30.4zM55.1 480l-23-99.6 107.4-46 59.5 72.8c103.6-48.6 159.7-104.9 208.1-208.1l-72.8-59.5 46-107.4 99.6 23C479.7 289.7 289.6 479.7 55.1 480z" class=""></path></svg>
        </div>
        <div class="text-people-comun">
          <p><?php the_sub_field('telefone_capela_igreja') ?></p>
        </div>
      </li>
      <?php endif; ?>

    </ul>
  </div> <!-- close-item-comun -->

  <?php
  endwhile;
  else :
    // no rows found
  endif;
  ?>
</div>
<?php endif; ?>




<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'posts_to_igrejas',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
  'orderby'=> 'date',
  'order' => 'ASC'
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>

<?php include('template-parts/conection-padre.php') ?>

<?php 
// Prevent weirdness
wp_reset_postdata(); endif;
?>


<?php
// Find connected pages
$connected = new WP_Query( array(
  'connected_type' => 'igrejas_to_comun',
  'connected_items' => get_queried_object(),
  'nopaging' => true,
  'orderby'=> 'date',
  'order' => 'ASC'
) );

// Display connected pages
if ( $connected->have_posts() ) :
?>

<?php include('template-parts/conection-instituicoes.php') ?>

<?php 
// Prevent weirdness
wp_reset_postdata(); endif;
?>




<?php endwhile; ?>
<?php get_footer() ?>
