<?php get_header(); ?>

<!-- Data de aniversário e ordenação -->

<div class="header-content-app header-content-app-print">
  <h3>Relatório</h3>
  <button class="btn btn-light btn-sm" onclick="printFunction()">
    <i class="fas fa-print"></i> 
    Imprimir
  </button>
</div>

<div class="container-item-app container-report">

  <table id="dataTable" style="width:100%">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Data de Nascimento</th>
            <th>Data de Ordenação</th>
            <th>Tempo de ordenação</th>
        </tr>
    </thead>
    <tbody>
        <?php
          $quero_relatorio = new WP_Query(
            array(
              'post_type' => 'post',
              'posts_per_page' => -1,
              'orderby'=> 'title',
              'order' => 'ASC'
            )
          );
          if($quero_relatorio->have_posts()): while($quero_relatorio->have_posts()): $quero_relatorio->the_post();
        ?>
        <tr>
            <td><?php the_title(); ?></td>
            <td class="tableNasc">
              <?php if ( have_rows('nascimento_general') ) : ?>
                <?php while( have_rows('nascimento_general') ) : the_row(); ?>
                  <?php the_sub_field('data_de_nascimento'); ?>
                <?php endwhile; ?>
              <?php endif; ?>
            </td>
            <td class="tableOrd">
              <?php if ( have_rows('ordenacao_general') ) : ?>
                <?php while( have_rows('ordenacao_general') ) : the_row(); ?>
                  <?php the_sub_field('data_de_ordenacao'); ?>
                <?php endwhile; ?>
              <?php endif; ?>
            </td>
            <td>
              <?php if ( have_rows('ordenacao_general') ) : ?>
                <?php while( have_rows('ordenacao_general') ) : the_row(); ?>
                  <?php 
                  $rest = substr(get_sub_field('data_de_ordenacao'), 6,10); 
                  echo date('Y') - $rest . " Anos";
                  ?>
                <?php endwhile; ?>
              <?php endif; ?>
            </td>
        </tr>
        <?php endwhile; wp_reset_query(); endif; ?>
    </tbody>
    <tfoot>
        <tr>
            <th>Nome</th>
            <th>Data de Nascimento</th>
            <th>Data de Ordenação</th>
            <th>Tempo de ordenação</th>
        </tr>
    </tfoot>
  </table>


</div>

<?php get_footer(); ?>