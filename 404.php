<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>404 - Página não encontrada</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
  <style>

    * {
      padding: 0;
      margin: 0;
      box-sizing: border-box;
    }

    html {
      font-family: 'Roboto', sans-serif;
      height: 100%;
      text-align: center;
    }

    .wrapper{
      width: 100%;
      height: 100%;
      overflow-y: auto;
      display: flex;
      align-items: baseline;
      justify-content: center;
      background-color: #F9F9F9;
    }

    .wrapper .wrapper-content{
      position: relative;
      width: 100%;
      max-width: 620px;
      height: auto;
      display: flex;
      flex-direction: column;
      flex: 1;
      align-items: center;
      justify-content: center;
      padding: 30px 15px;
      border-radius: 6px;
    }

    h1 {
      font-size: 8em;
      line-height: 1.2;
      color: rgba(38, 51, 69, 1);
    }

    h1 span{
      font-size: 1.6rem;
      display: block;

    }

    .content-wrapper {

      max-width: 580px;
      background-color: $white;
      border-radius: 6px;
      padding: 15px;
      margin: 60px 0;
      box-shadow: 0 3px 14px rgba(0,0,0,.14);

    }

    p {

      color: #777;
      text-align: left;
      width: 100%;
      padding-left: 30px;
      font-size: 1rem;
      font-weight: 300;

    }

    p i {

      width: 30px;
      margin-left: -30px;
      padding-right: 10px;

    }

    .logo img {

      width: 52px;
      height: auto;

    }

    .logo h2 {
      color: rgba(38, 51, 69, 1);
      font-size: 2.2rem;
      font-weight: 300;
      text-transform: uppercase;
    }
    .logo h2 span{
      font-size: .7rem;
      display: block;
      font-weight: 500
    }


  </style>

</head>
<body class="wrapper">
  <div class="wrapper-content">
    <h1><strong>404</strong><span>Página não encontrata</span></h1>
    <div class="content-wrapper">
      <p><i class="fa fa-info-circle" aria-hidden="true"></i>Essa não é a pagina que você está procurando.</p>
      <p>Verifique se o link da página foi digitado corretamente. Evite utilizar espaços, caracteres especiais ou letras maiúsculas.</p>
      <p>Clique aqui para voltar para a <a href="<?php esc_html( home_url() ) ?>/">página inicial</a>.</p>
    </div>

    <div class="logo">
      <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-arquidiocese.png" alt="Logo Arquidiocese de Brasília – DF">
      <h2>Anuário <span>Arquidiocesano de Brasília</span></h2>
    </div>
  </div>

  <script src="https://use.fontawesome.com/46fcbbfb8a.js"></script>
</body>
</html>
<!-- IE needs 512+ bytes: https://blogs.msdn.microsoft.com/ieinternals/2010/08/18/friendly-http-error-pages/ -->