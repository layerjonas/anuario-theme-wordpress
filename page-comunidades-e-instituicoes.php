<?php get_header(); ?>

<div class="header-content-app">
  <h3>Comunidades e Instituições</h3>
</div>

<div class="container-item-app">

  <ul class="item-app item-load">
    <?php
      $query_padres_general2 = new WP_Query(
        array(
          'post_type' => 'comun_e_instituicoes',
          'posts_per_page' => -1,
          'orderby'=> 'title',
          'order' => 'ASC'
        )
      );
      if($query_padres_general2->have_posts()): while($query_padres_general2->have_posts()): $query_padres_general2->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-comun' ); ?>
    <?php endwhile; wp_reset_query(); endif; ?>
  </ul> 

  <button class="btn load-more">Abrir mais <svg aria-hidden="true" data-prefix="far" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-3x"><path fill="currentColor" d="M441.9 167.3l-19.8-19.8c-4.7-4.7-12.3-4.7-17 0L224 328.2 42.9 147.5c-4.7-4.7-12.3-4.7-17 0L6.1 167.3c-4.7 4.7-4.7 12.3 0 17l209.4 209.4c4.7 4.7 12.3 4.7 17 0l209.4-209.4c4.7-4.7 4.7-12.3 0-17z" class=""></path></svg></button>

</div>

<?php get_footer(); ?>
