

var $ =jQuery.noConflict();

$('document').ready(function() {
      
  $('.form-feedback .send').attr("value", "");

  var wpcf7Elm = document.querySelector( '.wpcf7' );
  wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
    $('.form-help p').hide();
    $('.form-help').css({"height": "auto"});
    $('.form-feedback-2 p').hide();
    $('.form-feedback-2').css({"height": "auto"});
  }, true );

  wpcf7Elm.addEventListener( 'wpcf7invalid', function( event ) {
    $('.wpcf7-response-output').css({
      "height":"0px"
    });
  }, true );

  wpcf7Elm.addEventListener( 'wpcf7spam', function( event ) {
    $('.form-help p').hide();
    $('.form-help').css({"height": "auto"});
    $('.form-feedback-2 p').hide();
    $('.form-feedback-2').css({"height": "auto"});
  }, true );

  wpcf7Elm.addEventListener( 'wpcf7mailfailed', function( event ) {
    $('.form-help p').hide();
    $('.form-help').css({"height": "auto"});
    $('.form-feedback-2 p').hide();
    $('.form-feedback-2').css({"height": "auto"});
  }, true );



  $('input[type=submit].send').hover(function(){
    $('.send-before').toggleClass('show-enviar');
  });


  // Help and feedback open

  $('.open-feedback').click(function(){
    $('.feedback').addClass('is-open');
      $('.app-nav-toggle').removeClass('toggle');
    $(".sidebar-app").removeClass("sidebar-app-toogle");
    $('.feedback form textarea').focus();
  });

  $('.close').click(function(){
    $('.feedback').removeClass('is-open');
  });

  $('.open-help').click(function(){
    $('.help').addClass('is-open');
      $('.app-nav-toggle').removeClass('toggle');
    $(".sidebar-app").removeClass("sidebar-app-toogle");
    $('.help form textarea').focus();
  });

  $('.close').click(function(){
    $('.help').removeClass('is-open');
  });

  $('.header-app').click(function(){
    $('.help, .feedback').removeClass('is-open');
  });

  //slick

  // $('.vicariatos').slick({
  //   slidesToShow: 4,
  //   slidesToScroll: 1,
  //   autoplay: true,
  //   autoplaySpeed: 1000,
  //   arrows: false
  // });

  // app sidebar

  $(".app-nav-toggle").on("click", function(){
    $(".sidebar-app").toggleClass("sidebar-app-toogle");
  });


  // toggle nav

  $('.mobile-nav-toggle').click(function(e) {
      $(this).toggleClass('toggle');
      $('.site-nav').toggleClass('show-nav');
      $('body').toggleClass('no-scroll');
      $('.header').toggleClass('bg-header');
  });

  $('.app-nav-toggle').click(function(){
    $('.app-nav').toggleClass('show-app-nav');
    $(this).toggleClass('toggle');
  });


  // search

  $('.btn-search').click(function(){
    $('.search-input').toggleClass('show-search');
    $('.search form').toggleClass('modify-color-search');
    $('.search form').toggleClass('form-position');
    $('.search-input').focus();
  });

  $('.search-input').focus(function(){
    $('.search form').toggleClass('form-opacity');
  });

  $('.search-input').blur(function(){
    $('.search form').toggleClass('form-opacity');
  });

  $('.btn-close-input').click(function(){
    $('.search form').removeClass('modify-color-search');
    $('.search-input').removeClass('show-search');
  });

  // header scrollTop

  $(window).on('scroll', function(){
    if ($('.toggle').is(':visible')){
      $(this).scrollTop(0);
      $('.header').css("background-color", "");
    }
    if ($(this).scrollTop() > 40){
      $('.header').addClass('header-modify');
    } else {
      $('.header').removeClass('header-modify');
    }
  });

  // SCROLL SUAVE

  $('a.see-more').click(function(e){
    e.preventDefault();
    var i = $(this).attr('href'),
        menuHeight = $('header').innerHeight(),
        targetOffset = $(i).offset().top;
    $('html, body').animate({
      scrollTop: targetOffset - menuHeight
    }, 800);
  });


  // textToggle

  jQuery.fn.extend({
    toggleText: function (a, b){
        var that = this;
            if (that.text() != a && that.text() != b){
                that.text(a);
            }
            else
            if (that.text() == a){
                that.text(b);
            }
            else
            if (that.text() == b){
                that.text(a);
            }
        return this;
    }
  });

  $("#toggle-more").on("click" , function(){
    $(".more").slideToggle();
    $("#toggle-more span").toggleText('Leia mais', 'Leia menos');
  });



});

// HISTORY BACK BUTTON
function goBack() {
    window.history.back();
}