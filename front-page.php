 <?php 
/* 
 Template Name: Home
*/

get_header(); ?>

<div class="container-item-app">

  <div class="header-hierarchy">
    <p>Destaque</p>
  </div>

  <ul class="item-app item-load">
    <?php
      $query_padres_general = new WP_Query(
        array(
          'posts_per_page' => -1,
          'tag' => 'destaque',
          'orderby'=> 'date',
          'order' => 'ASC'
        )
      );
      if($query_padres_general->have_posts()): while($query_padres_general->have_posts()): $query_padres_general->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-padre' ); ?>
    <?php endwhile;?> 
    <?php endif; ?>
  </ul>
  
  <div class="header-hierarchy">
    <p>Bispos Auxiliares</p>
  </div>

  <ul class="item-app item-load">
    <?php
      $query_padres_general = new WP_Query(
        array(
          'posts_per_page' => 10,
          'category_name' => 'bispos-auxiliares',
          'orderby'=> 'date',
          'order' => 'ASC'
        )
      );
      if($query_padres_general->have_posts()): while($query_padres_general->have_posts()): $query_padres_general->the_post();
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-padre' ); ?>
    <?php endwhile; wp_reset_query(); endif; ?>
  </ul>


</div>

<?php get_footer(); ?>
