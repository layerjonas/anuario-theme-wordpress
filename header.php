<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  

  <link rel="manifest" href="/manifest.json">
  <meta name="theme-color" content="#263345">
  <meta name="msapplication-TileColor" content="#263345" />
  <meta name="application-name" content="Anuario" />

  <meta name="mobile-web-app-capable" content="yes">

  <link rel="icon" sizes="192x192" href="/images/b5446447-334d-666a-0274-e244f634f943.webPlatform.png">
  <link rel="icon" sizes="128x128" href="/images/83b76e9f-328f-229c-7528-c86234a00179.webPlatform.png">
  <link rel="apple-touch-icon" sizes="128x128" href="/images/83b76e9f-328f-229c-7528-c86234a00179.webPlatform.png">
  <link rel="apple-touch-icon-precomposed" sizes="128x128" href="/images/83b76e9f-328f-229c-7528-c86234a00179.webPlatform.png">

  <?php wp_head(); ?>
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>

<body <?php body_class() ?>>

<header class="header-site">
  <div class="container-fluid d-flex">
    <div class="col-ico-menu">
      <!-- <div class="app-nav-toggle">
        <span></span>
      </div> -->

      <div id="nav-icon3" class="app-nav-toggle">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>

    </div>
    <div class="col-brand">
      <h1>
        <a href="<?php echo esc_url( home_url('/') ) ?>">
          <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-arquidiocese.png" alt="Logo Arquidiocese de Brasília – DF"> Anuário  <span>ARQUIDIOCESANO DE BRASÍLIA</span>
        </a>
      </h1>
    </div>
    <div class="col-search">
      <form method="get" action="<?php echo home_url('/'); ?>">
        <svg class="close-search" aria-hidden="true" data-prefix="far" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-times fa-w-12 fa-3x ico-close-search"><path fill="currentColor" d="M231.6 256l130.1-130.1c4.7-4.7 4.7-12.3 0-17l-22.6-22.6c-4.7-4.7-12.3-4.7-17 0L192 216.4 61.9 86.3c-4.7-4.7-12.3-4.7-17 0l-22.6 22.6c-4.7 4.7-4.7 12.3 0 17L152.4 256 22.3 386.1c-4.7 4.7-4.7 12.3 0 17l22.6 22.6c4.7 4.7 12.3 4.7 17 0L192 295.6l130.1 130.1c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17L231.6 256z" class=""></path></svg>
        <input type="text" name="s" value="" class="input-search" placeholder="Pesquisar Padres, Igrejas, etc..." required>
        <button type="submit"><i class="fas fa-search"></i></button>
      </form>
      <button type="button" class="ico-search"><i class="fas fa-search"></i></button>
    </div>
  </div>
</header>

<?php if(is_single()) : ?>
<section class="app app-single">
<?php else : ?>
<section class="app">
<?php endif; ?>

<aside class="sidebar-app">
  <nav>
    <ul class="posttype-menu-sidebar-app">
      <li>
        <a href="<?php echo esc_html( home_url() ) ?>/">
          <div class="ico-sidebar">
            <svg width="32px" height="32px" aria-hidden="true" data-prefix="fal" data-icon="book" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-book fa-w-16 fa-1x"><path fill="currentColor" d="M356 160H188c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12zm12 52v-8c0-6.6-5.4-12-12-12H188c-6.6 0-12 5.4-12 12v8c0 6.6 5.4 12 12 12h168c6.6 0 12-5.4 12-12zm64.7 268h3.3c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12H80c-44.2 0-80-35.8-80-80V80C0 35.8 35.8 0 80 0h344c13.3 0 24 10.7 24 24v368c0 10-6.2 18.6-14.9 22.2-3.6 16.1-4.4 45.6-.4 65.8zM128 384h288V32H128v352zm-96 16c13.4-10 30-16 48-16h16V32H80c-26.5 0-48 21.5-48 48v320zm372.3 80c-3.1-20.4-2.9-45.2 0-64H80c-64 0-64 64 0 64h324.3z" class=""></path></svg>
          </div>
          <div class="name-item-sidebar">
            <span>Anuário</span>
          </div>
        </a>
      </li>
      <li>
        <a href="<?php echo esc_html( home_url() ) ?>/clero">
          <div class="ico-sidebar">
            <svg width="32" height="32" viewBox="0 0 100 127" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="#263345" xlink:href="#a"/><defs><path id="a" d="M49.626 65.541c-8.505 0-17.869-11.018-19.178-21.652a4.843 4.843 0 0 1-1.256-1.518 4.744 4.744 0 0 1-.539-1.88s-2.261-7.894 1.047-6.36c-2.318-11.985-.56-16.06-.56-16.06C32.11 9.026 42.39 4.678 42.39 4.678c2.28-2.43 2.318-1.608 2.131-.895a8.582 8.582 0 0 1 1.253-1.243 8.917 8.917 0 0 0-1.384 1.827l1.14.548a8.322 8.322 0 0 1 1.421-1.735 8.736 8.736 0 0 0-1.177 1.827l.598.347A6.506 6.506 0 0 1 49.494 1.7 7.291 7.291 0 0 1 51.57 0a6.517 6.517 0 0 0-2.038 3.015A7.314 7.314 0 0 1 52.13.914c0 2.394 2.598 4.55 3.888 5.482-.262-.603-.467-.987-.467-.987.444.45.845.939 1.195 1.462l.561.128a8.704 8.704 0 0 0-.934-1.152c.504.439.916.84 1.308 1.225l1.234.31a5.303 5.303 0 0 0-.393-.493c.275.185.537.386.785.603a19.71 19.71 0 0 1 9.944 5.902c6.187 6.815 2.991 17.779 2.038 20.574 2.766-.786.673 6.523.673 6.523a4.815 4.815 0 0 1-.536 1.875 4.911 4.911 0 0 1-1.24 1.523c-1.514 10.068-10.804 21.652-20.56 21.652zM47.999 4.622a6.474 6.474 0 0 1 1.14-2.558 5.606 5.606 0 0 0-1.495 2.412l.356.146zm25.776 62.764S90.15 72.429 92.84 78.24c0 0 8.935 19.953 6.842 37.568 0 0-.075 1.461-3.066 3.289a133.944 133.944 0 0 1-48.878 7.856s-27.626-1.352-45.14-9.099c0 0-2.485-.585-2.598-6.212 0 0 .897-24.649 8.542-35.704a17.09 17.09 0 0 1 7.346-4.86s17.664-5.244 18.86-9.703c0 0 13.047 33.09 30.224 0 0 0 0 2.997 8.804 6.012zM54.093 109.43h8.393v-7.126h-8.393v-8.113h-7.29v8.113h-8.485v7.126h8.486v8.368h7.29v-8.368z" fill-rule="evenodd"/></defs></svg>
          </div>
          <div class="name-item-sidebar">
            <span>Clero</span>
          </div>
        </a>
      </li>
      <li>
        <a href="<?php echo esc_html( home_url() ) ?>/igrejas">
          <div class="ico-sidebar">
            <svg width="30" height="30" viewBox="0 0 100 106" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use fill="#263345" xlink:href="#b"/><defs><path id="b" d="M48.006 0h3.872v7.373h8.18v3.856h-8.196v6.76l-1.936 1.936-1.936-1.936v-6.744h-8.164V7.373h8.18V0zM31.049 64.535L1.218 76.587c-2.146.807-1.258 3.34 1.113 2.388l3.227-1.307v28.137h35.494V78.459l8.97-9.067 9.1 9.083v27.33h35.333V77.668l3.227 1.307c2.371.968 3.226-1.613 1.113-2.388L69.012 64.454V51.95l-19.07-19.102-18.91 18.925V64.47l.017.065zm-4.84 4.84v36.56-36.56zm66.148 7.583L69.01 67.294l22.958 9.357.484.194-.097.113zM49.925 23.394L25.353 48.062a2.104 2.104 0 1 0 2.985 2.969L49.94 29.41v-6.017l24.572 24.668a2.104 2.104 0 1 1-2.985 2.968L49.94 29.411v-6.018h-.016zM14.269 82.492l4.001-4.033 4.033 4.033v12.133h-8.066V82.492h.032zm31.735-34.107l4.017-4.033 4.034 4.033v12.133h-8.067V48.385h.016zm31.735 34.107l4.001-4.033 4.034 4.033v12.133h-8.067V82.492h.032z" fill-rule="evenod"/></defs></svg>
          </div>
          <div class="name-item-sidebar">
            <span>Igrejas</span>
          </div>
        </a>
      </li>
      <li>
        <a href="<?php echo esc_html( home_url() ) ?>/comunidades-e-instituicoes/">
          <div class="ico-sidebar">
            <i class="far fa-building"></i>
          </div>
          <div class="name-item-sidebar">
            <span>Comunidades e Instituições</span>
          </div>
        </a>
      </li>
      <li>
        <a href="<?php echo esc_html( home_url() ) ?>/pastorais-e-movimentos">
          <div class="ico-sidebar">
            <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g clip-path="url(#clip-0)" transform="translate(-3735 1457)"><g fill="#263345"><use xlink:href="#path2_fill" transform="matrix(-1 0 0 1 3765 -1452.52)"/><use xlink:href="#path3_fill" transform="matrix(-1 0 0 1 3748.56 -1452.52)"/><use xlink:href="#path4_fill" transform="matrix(-1 0 0 1 3756.31 -1457)"/></g></g><defs><path id="path0_fill" d="M0 0h323v983H0V0z"/><path id="path1_stroke_2x" d="M0 0v-2h-2v2h2zm323 0h2v-2h-2v2zm0 983v2h2v-2h-2zM0 983h-2v2h2v-2zM0 2h323v-4H0v4zm321-2v983h4V0h-4zm2 981H0v4h323v-4zM2 983V0h-4v983h4z"/><path id="path2_fill" d="M10.361 7.202a2.262 2.262 0 1 0-3.66 2.658l1.65 2.272a.754.754 0 0 1-1.108-.052L4.521 8.804V2.262A2.263 2.263 0 0 0 2.261 0 2.26 2.26 0 0 0 0 2.262v8.032c0 .513.174 1.012.495 1.414l5.039 6.301c.32.402.494.9.494 1.414v.184h7.536v-6.033a6.046 6.046 0 0 0-1.206-3.62L10.36 7.202zm1.69 10.897H7.296a3.672 3.672 0 0 0-.584-1.032l-5.04-6.302a.746.746 0 0 1-.164-.471V2.262a.732.732 0 0 1 .221-.532.753.753 0 0 1 1.286.532v7.089l.349.42 2.722 3.27a2.257 2.257 0 0 0 3.315.17l.928-.91-.763-1.05-1.648-2.272a.755.755 0 0 1 1.22-.886l2.01 2.771a4.55 4.55 0 0 1 .905 2.715v4.52z"/><path id="path3_fill" d="M11.303 0a2.26 2.26 0 0 0-2.26 2.262v6.542L6.32 12.075a.752.752 0 0 1-1.107.052l1.649-2.272a2.264 2.264 0 0 0-2.184-3.563 2.26 2.26 0 0 0-1.475.905L1.206 9.95A6.06 6.06 0 0 0 0 13.574v6.033h7.535v-.184c0-.514.174-1.012.495-1.414l5.04-6.301c.32-.402.494-.9.494-1.414V2.262A2.263 2.263 0 0 0 11.303 0zm.749 10.289a.728.728 0 0 1-.16.476l-5.04 6.302c-.249.31-.446.658-.584 1.032h-4.76v-4.525a4.57 4.57 0 0 1 .917-2.734l1.997-2.752a.746.746 0 0 1 .613-.311.754.754 0 0 1 .608 1.197l-1.65 2.272-.762 1.05.928.91a2.255 2.255 0 0 0 3.315-.17l2.722-3.27.349-.42V2.258a.754.754 0 0 1 1.286-.533.76.76 0 0 1 .22.533v8.03z"/><path id="path4_fill" d="M11.03 7.023c1.026-.996 1.639-2.25 1.639-3.353 0-1.114-.352-2.046-1.016-2.695C11 .338 10.072 0 8.972 0 7.922 0 6.97.603 6.334 1.137 5.444.393 4.534 0 3.697 0 2.575 0 1.637.35.983 1.01.34 1.66 0 2.58 0 3.672c0 1.77 1.482 3.2 1.648 3.354L5.53 10.84a1.151 1.151 0 0 0 1.61 0l3.882-3.806.008-.011zM6.333 9.716L2.6 6.046v-.003c-.613-.573-1.232-1.528-1.232-2.371-.002-1.486.826-2.305 2.329-2.305 1.002 0 2.025 1.037 2.46 1.482l.094.094.083.084.083-.083.099-.098c.478-.48 1.467-1.48 2.456-1.48 1.503 0 2.331.82 2.331 2.306 0 .347-.121 1.276-1.241 2.385l-3.728 3.66z"/></defs></svg>
          </div>
          <div class="name-item-sidebar">
            <span>Pastorais e Movimentos</span>
          </div>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url() ?>/frequentes">
          <div class="ico-sidebar">
            <i class="fas fa-history"></i>
          </div>
          <div class="name-item-sidebar">
            <span>Frequentes</span>
          </div>
        </a>
      </li>
      <?php if(current_user_can('administrator') || current_user_can('editor')) : ?>
      <li>
        <a href="<?php echo home_url() ?>/relatorio">
          <div class="ico-sidebar">
            <i class="fas fa-clipboard-list"></i>
          </div>
          <div class="name-item-sidebar">
            <span>Relatório</span>
          </div>
        </a>
      </li>
      <?php endif; ?>
      <?php if(current_user_can('administrator') || current_user_can('editor')) : ?>
      <li>
        <a href="<?php echo home_url() ?>/wp-admin">
          <div class="ico-sidebar">
            <i class="fas fa-sliders-h"></i>
          </div>
          <div class="name-item-sidebar">
            <span>Painel</span>
          </div>
        </a>
      </li>
      <?php endif; ?>
      <?php if(current_user_can('subscriber')) : ?>
      <li>
        <a href="<?php echo home_url() ?>/wp-admin/profile.php">
          <div class="ico-sidebar">
            <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" data-prefix="far" data-icon="user-edit" viewBox="0 0 640 512" class="svg-inline--fa fa-user-edit fa-w-20 fa-1x"><path fill="currentColor" d="M358.9 433.3l-6.8 61c-1.1 10.2 7.5 18.8 17.6 17.6l60.9-6.8 137.9-137.9-71.7-71.7-137.9 137.8zM633 268.9L595.1 231c-9.3-9.3-24.5-9.3-33.8 0l-41.8 41.8 71.8 71.7 41.8-41.8c9.2-9.3 9.2-24.4-.1-33.8zM223.9 288c79.6.1 144.2-64.5 144.1-144.1C367.9 65.6 302.4.1 224.1 0 144.5-.1 79.9 64.5 80 144.1c.1 78.3 65.6 143.8 143.9 143.9zm-4.4-239.9c56.5-2.6 103 43.9 100.4 100.4-2.3 49.2-42.1 89.1-91.4 91.4-56.5 2.6-103-43.9-100.4-100.4 2.3-49.3 42.2-89.1 91.4-91.4zM134.4 352c14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 16.7 0 32.2 5 45.5 13.3l34.4-34.4c-22.4-16.7-49.8-26.9-79.9-26.9-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h258.3c-3.8-14.6-2.2-20.3.9-48H48v-25.6c0-47.6 38.8-86.4 86.4-86.4z"/></svg>
          </div>
          <div class="name-item-sidebar">
            <span>Editar Perfil</span>
          </div>
        </a>
      </li>
      <?php endif; ?>
    </ul>

    <hr>

    <div id="users">
      <input class="search" placeholder="Pesquisar Categorias" />
      <ul class="app-nav-vicariatos list">
        <?php wp_list_categories( array(
          'title_li' => ''
          ) ); ?>
      </ul>
    </div>

    <hr>

    <ul class="list-sidebar-footer">
      <li>
        <a href="javascript:" class="link-feedback">
          <div class="ico-sidebar">
            <i class="far fa-comments"></i>
          </div>
          <div class="name-item-sidebar">
            Enviar sua opnião ou relatar um erro
          </div>
        </a>
      </li>

      <li>
        <a href="#" class="link-help">
          <div class="ico-sidebar">
            <i class="far fa-question-circle"></i>
          </div>
          <div class="name-item-sidebar">
            Ajuda
          </div>
        </a>
      </li>

      <li>
        <a href="<?php echo wp_logout_url(); ?>">
        <div class="ico-sidebar">
          <svg style="top: -5px" aria-hidden="true" data-prefix="far" data-icon="sign-out-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sign-out-alt fa-w-16 fa-3x"><path fill="currentColor" d="M272 112v51.6h-96c-26.5 0-48 21.5-48 48v88.6c0 26.5 21.5 48 48 48h96v51.6c0 42.6 51.7 64.2 81.9 33.9l144-143.9c18.7-18.7 18.7-49.1 0-67.9l-144-144C323.8 48 272 69.3 272 112zm192 144L320 400v-99.7H176v-88.6h144V112l144 144zM96 64h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-53 0-96-43-96-96V160c0-53 43-96 96-96z" class=""></path></svg>
        </div>
        <div class="name-item-sidebar">
          Sair do Anuário
        </div>
        </a>
      </li>
    </ul>


    <footer class="footer-sidebar">
      <p>© <?php the_time('Y') ?> • Arquidiocese de Brasília. Some Rights Reserved.</p>
      <p class="made-with"><a href="https://www.hugolsdias.com" target="_blank">Made with <svg class="svg-inline--fa fa-heart fa-w-16" aria-hidden="true" data-prefix="far" data-icon="heart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"></path></svg> in Brasília/DF by <span>hugo dias</span></a>.</p>
    </footer>

  </nav>
</aside>

<?php // if(is_single()) : ?>
<!-- <div class="wrapper-app-single"> -->
<?php //else : ?>
<div class="content-app">
<?php// endif; ?>


<div class="modal-form modal-feedback">
  <div class="modal-form__shadow form-exit"></div>
  <div class="modal-form__container animated">
    <div class="modal-form__content">
      <p class="modal-form__title">Enviar Feedback</p>

      <?php
        //echo do_shortcode('[contact-form-7 id="113" title="Feedback"]')
        echo do_shortcode('[contact-form-7 id="2529" title="Form Feedback"]')
      ?>

      <div class="modal-form__footer">
        <p>Seu feedback, suas informações adicionais serão enviados à equipe responsável. Desde já, gostariamos de agradecer por nos relatar este problema ou por compartilhar suas ideias conosco.</p>
      </div>
    </div>
  </div>
</div>

<div class="modal-form modal-help">
  <div class="modal-form__shadow form-exit"></div>
  <div class="modal-form__container animated">
    <div class="modal-form__content">
      <p class="modal-form__title">Enviar mensagem</p>

      <?php
        // echo do_shortcode('[contact-form-7 id="113" title="Feedback"]')
        echo do_shortcode('[contact-form-7 id="2530" title="Form Feedback_copy"]')
      ?>

      <div class="modal-form__footer">
        <p>Sua mensagem, suas informações adicionais serão enviados à equipe responsável. Desde já, gostariamos de agradecer por entrar em contato conosco.</p>
      </div>
    </div>
  </div>
</div>
