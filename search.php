<?php get_header(); global $wp_query; ?>


<div class="header-content-app" style="margin-bottom: 30px;">
  <h3>Você pesquisou: <?php echo get_search_query(); ?></h3>
</div>

<div class="container-item-app">

  <?php 
    $query_search_padre = new WP_Query( array( 'post_type' => 'post', 's' => $s, 'posts_per_page' => 10 ) );
    if($query_search_padre->have_posts()):
  ?>
  <div class="header-hierarchy" style="display: block">
    <p>Clero</p>
  </div>
  <ul class="item-app">
    <?php 
       while($query_search_padre->have_posts()): $query_search_padre->the_post() ;
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-padre' ); ?>
    <?php 
      endwhile;
    ?>
  </ul>
  <?php endif; wp_reset_query();?>

  <?php 
    rewind_posts();
    $query_search_igreja = new WP_Query( array( 'post_type' => 'igreja', 's' => $s, 'posts_per_page' => 10 ) );
    if($query_search_igreja->have_posts()): 
  ?>
  <div class="header-hierarchy" style="display: block">
    <p>Igrejas</p>
  </div>
  <ul class="item-app">
    <?php 
      while($query_search_igreja->have_posts()): $query_search_igreja->the_post() ;
    ?>
    <?php get_template_part( 'template-parts/component', 'loop-igreja' ); ?>
    <?php 
      endwhile;
    ?>
  </ul>
  <?php endif; wp_reset_query(); ?>

  <?php 
    rewind_posts();
    $query_search_comu = new WP_Query( array( 'post_type' => 'comun_e_instituicoes', 's' => $s, 'posts_per_page' => 10) );
    if($query_search_comu->have_posts()): 
  ?>
  <div class="header-hierarchy" style="display: block">
    <p>Comunidades e Instituições</p>
  </div>
  
  <ul class="item-app">
    <?php 
      while($query_search_comu->have_posts()): $query_search_comu->the_post() ;
    ?>

    <?php get_template_part( 'template-parts/component', 'loop-comun' ); ?>
    
    <?php 
      endwhile;
    ?>
  </ul>
  <?php endif; wp_reset_query(); ?>


  <?php 
    rewind_posts();
    $query_search_pastorais = new WP_Query( array( 'post_type' => 'pastorais_e_moviment', 's' => $s, 'posts_per_page' => 10) );
    if($query_search_pastorais->have_posts()): 
  ?>
  <div class="header-hierarchy" style="display: block">
    <p>Pastorais e Movimentos</p>
  </div>
  <ul class="item-app">
    <?php 
      while($query_search_pastorais->have_posts()): $query_search_pastorais->the_post() ;
    ?>

    <?php get_template_part( 'template-parts/component', 'loop-pastorais' ); ?>
    
    <?php 
      endwhile;
    ?>
  </ul>
  <?php endif; wp_reset_query(); ?>

  <?php 
    rewind_posts();
    $query_search_empty = new WP_Query( array( 
      'post_type' => array('post','igreja','comun_e_instituicoes', 'pastorais_e_moviment'), 
      's' => $s, 'posts_per_page' => 10 ) 
    );
    if($query_search_empty->have_posts()): 
  ?>
  <?php else: ?>
  <ul class="item-app">
  <?php get_template_part( 'template-parts/component', 'loop-empty' ); ?>
  </ul>

  <?php endif; ?>

</div> 

<?php get_footer(); ?>
